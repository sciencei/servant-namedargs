# Revision history for servant-namedargs

## 0.1.0.0 -- 2019-01-23

* First version.

## 0.1.0.1 -- 2019-01-25

* Relaxation of base constraint and some minor documentation fixes; had to be done as a new version to be able to build on Hackage

## 0.1.1.0 -- 2019-02-25

* Introduction of NamedBody' combinator

## 0.1.1.1 -- 2019-03-22

* Deps bump for servant 0.16
